#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from confijector.ConfiguredBinder import ConfiguredBinder
from injector import Injector

sample_config = {
    "foo": {
        "bar": "baz"
    },
    "hoo": {
        "bee": "boo"
    }
}


class Foo:
    def __init__(self, bar: str):
        self.bar = bar


class Hoo:
    def __init__(self, bee: str):
        self.bee = bee


class FooChild(Foo):
    def __init__(self, bar: str):
        super().__init__(bar)


class TestConfiguredBinder:
    def test_configured(self):
        foo_module = ConfiguredBinder(bind4=Foo, config=sample_config["foo"])
        injector = Injector([foo_module])
        foo = injector.get(Foo)
        assert foo.bar == sample_config["foo"]["bar"]

    def test_bind_child(self):
        foochild_module = ConfiguredBinder(bind4=Foo, bind2=FooChild, config=sample_config["foo"])
        injector = Injector([foochild_module])
        foo = injector.get(Foo)
        assert foo.bar == sample_config["foo"]["bar"]
        assert isinstance(foo, FooChild)

    def test_bind_parent(self):
        try:
            foo_module = ConfiguredBinder(bind4=FooChild, bind2=Foo, config=sample_config["foo"])
        except Exception as e:
            assert isinstance(e, ValueError), "Expected ValueError, but actual {0}".format(e)
            assert str(e).startswith("Cannot bind parent to child"), \
                "Expected message to start with 'Cannot bind parent to child', but actual {0}".format(str(e))

    # TODO: add test to override binder in injector

    # TODO: add test for multi-binder in single injector
    def test_multi_binder(self):
        foo_module = ConfiguredBinder(bind4=Foo, config=sample_config["foo"])
        hoo_module = ConfiguredBinder(bind4=Hoo, config=sample_config["hoo"])

        raise NotImplementedError()