#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import abc
from injector import Binder, inject, Injector, Module, provider, singleton
from typing import Callable, Dict, Tuple, Union


class InjectorFactory(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def get_injector(self) -> Injector:
        raise NotImplementedError()

    @abc.abstractmethod
    def gen_modules(config: Dict) -> Union[Tuple[Callable, Module], None]:
        raise NotImplementedError()

    @abc.abstractmethod
    def gen_bind_func(config: Dict) -> Union[Tuple[Callable, Module], None]:
        raise NotImplementedError()


class ConfiguredBinder(Module):
    # TODO: Add callable to bind2
    def __init__(self, bind4: type, bind2: type = None, config: Dict = None):
        self.bind4 = bind4
        if bind2 is None:
            bind2 = bind4
        self.bind2 = bind2
        # TODO: Need to check lineage of bind4 and bind2
        if not issubclass(bind2, bind4):
            raise ValueError(
                "Cannot bind parent to child; {0} is not equal to or subclass of {1}".format(bind2, bind4))
        self.bind_func = "gen_bind_func"
        self.config = config

    def configure(self, binder):
        if self.config is None:
            binder.bind(self.bind4, to=self.bind2())
        else:
            binder.bind(self.bind4, to=self.bind2(**self.config))

    def add_to_jnjector(self, injector: Injector = None) -> Injector:
        if injector is None:
            injector = Injector(self)
            return injector
            # injector.binder.install(self)
        return injector.create_child_injector(self)
