#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from concurrent.futures import ThreadPoolExecutor
from icecream import ic
from injector import Binder, inject, Injector, Module, provider, singleton
import json
from logear import AutoLogException
import os
from typing import Callable, Dict, List, Optional


@AutoLogException.adapt_autolog_exception()
class ConfigLoader:
    @staticmethod
    def get_default_conf_file() -> str:
        project_dir = '.'
        return "{0}{1}{2}{3}{4}".format(project_dir, os.path.sep, '.config', os.path.extsep, 'json')

    def read_config_file(self, conf_file: str) -> Dict:
        with open(conf_file, 'rt') as fd:
            content = fd.read()
        config = ic(json.loads(content))
        return config

    def _recursive_copy_dict(self, source: Dict, destination: Dict):
        for key, value in source.items():
            if key not in destination or not isinstance(value, dict):
                destination[key] = value
            else:
                if isinstance(destination[key], dict):
                    self._recursive_copy_dict(value, destination[key])
                else:
                    destination[key] = value

    def cascade_config_read(self, conf_files: Optional[List[str]] = None) -> Dict:
        if not conf_files or len(conf_files) < 1 or conf_files == [None]:
            conf_files = [ConfigLoader.get_default_conf_file()]

        with ThreadPoolExecutor() as executor:
            # Submit all tasks and store the future objects
            futures = [executor.submit(self.read_config_file, conf_file) for conf_file in ic(conf_files)]

            config = {}

            # Process the results in the order they were submitted
            for future in futures:
                result = future.result()
                self._recursive_copy_dict(result, config)

        return ic(config)

    @inject
    def __init__(self, conf_files: Optional[List[str]] = None):
        self.conf_files = conf_files

    def get_config(self) -> Dict:
        return self.cascade_config_read(conf_files=self.conf_files)


@AutoLogException.adapt_autolog_exception()
class ConfigDictModule(Module):
    @singleton
    @provider
    def provide_config_by_files(self, conf_files: List[str]) -> dict:
        conf_loader = ConfigLoader(conf_files=conf_files)
        return ic(conf_loader.get_config())


def config_bind_factory(conf_files: Optional[List[str]] = None) -> Callable[[Binder], None]:
    if conf_files is None:
        conf_files = []

    def bind_conf(binder: Binder) -> None:
        binder.multibind(List[str], to=conf_files)
    return bind_conf


def config_injector_factory(conf_files: Optional[List[str]] = None, parent: Optional[Injector] = None) -> Injector:
    func = config_bind_factory(conf_files=conf_files)
    module = ConfigDictModule()

    if parent is None:
        return Injector([func, module])
    else:
        return parent.create_child_injector([func, module])
