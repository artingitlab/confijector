#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from datetime import datetime
from icecream import ic
from injector import Injector
import json
import logging
from logear import AutoLogException
import mockito
import os
import pytest
import tempfile
import time
from typing import cast, Optional, Tuple

from confijector.ConfigLoader import config_bind_factory, config_injector_factory, ConfigDictModule, ConfigLoader


@AutoLogException.adapt_autolog_exception()
class TestConfigLoader:
    @pytest.fixture(scope="class", autouse=True)
    def logger_fixture(self) -> logging.Logger:
        logger = cast(logging.Logger, self.grab_logger())
        ic.configureOutput(outputFunction=(lambda s: logger.debug(s)))
        return logger

    @pytest.fixture(scope="class")
    def test_conf_file_fixture(self) -> str:
        project_dir = '.'
        return "{0}{1}{2}{1}{3}{1}{4}{5}{6}".format(
            project_dir, os.path.sep, 'tests', 'resource', 'test_config', os.path.extsep, 'json')

    @pytest.fixture(scope="class")
    def confloader_fixture(self, test_conf_file_fixture: str) -> ConfigLoader:
        return ConfigLoader(conf_files=[test_conf_file_fixture])

    def test_get_config(self, test_conf_file_fixture: str, confloader_fixture: ConfigLoader):
        with open(test_conf_file_fixture, 'rt') as fs:
            test_conf = json.load(fs)
        conf = confloader_fixture.get_config()
        assert conf == test_conf

    def prep_dummy_config_file(self, items: Optional[dict] = None) -> Tuple[str, dict]:
        timestamp = int(datetime.now().timestamp())
        dummy_key = f"dummy_key_{timestamp}"
        dummy_value = f"dummy_value_{timestamp}"
        dummy_conf = {dummy_key: dummy_value}
        if items:
            dummy_conf.update(items)

        dummy_conf_fd, dummy_conf_file = tempfile.mkstemp()
        with open(dummy_conf_fd, 'wt') as fs:
            fs.write(json.dumps(dummy_conf))

        return dummy_conf_file, dummy_conf

    def test_read_config_file(self, test_conf_file_fixture: str, confloader_fixture: ConfigLoader):
        test_data = {'Elon_Musk': 'DealBook_summit_2023'}
        dummy_conf_file, dummy_conf = self.prep_dummy_config_file(items=test_data)
        conf = confloader_fixture.read_config_file(conf_file=dummy_conf_file)
        assert conf[list(test_data.keys())[0]] == list(test_data.values())[0]
        assert len(conf) == 2
        assert conf == dummy_conf

    def test_recursive_copy_dict(self, confloader_fixture: ConfigLoader):
        dict1 = {'key1': {'subkey1': 'value1'}, 'key2': 'value2', 'key3': {'subkey3': 'value3'}}
        dict2 = {'key1': {'subkey1': 'different', 'subkey2': 'subvalue2'}, 'a': 1}
        confloader_fixture._recursive_copy_dict(source=dict2, destination=dict1)
        assert dict1['key1'] == dict2['key1']
        assert dict1['a'] == dict2['a']
        assert dict1['key2'] == 'value2'
        assert dict1['key3'] == {'subkey3': 'value3'}
        assert len(dict1) == 4

    def test_cascade_config_read(self, caplog, test_conf_file_fixture: str, confloader_fixture: ConfigLoader):
        # caplog.set_level(level=logging.DEBUG)

        dict1 = {'key1': {'subkey1': 'value1'}, 'key2': 'value2', 'key3': {'subkey3': 'value3'}}
        dict1_file, dict1_conf = self.prep_dummy_config_file(items=dict1)
        time.sleep(1)
        dict2 = {'key1': {'subkey1': 'different', 'subkey2': 'subvalue2'}, 'a': 1}
        dict2_file, dict2_conf = self.prep_dummy_config_file(items=dict2)

        conf = confloader_fixture.cascade_config_read(conf_files=[dict1_file, dict2_file])
        key = 'key1'
        assert conf[key] == dict2_conf[key]
        dict2_conf.pop(key)
        dict1_conf.pop(key)
        key = 'a'
        assert conf[key] == dict2_conf[key]
        dict2_conf.pop(key)
        key = 'key2'
        assert conf[key] == dict1_conf[key]
        dict1_conf.pop(key)
        key = 'key3'
        assert conf[key] == dict1_conf[key]
        dict1_conf.pop(key)
        assert len(conf) == 6
        key = list(dict1_conf.keys())[0]
        assert conf[key] == dict1_conf[key]
        key = list(dict2_conf.keys())[0]
        assert conf[key] == dict2_conf[key]

    def test_config_injection(self, test_conf_file_fixture: str):
        injector = Injector(modules=[config_bind_factory(conf_files=[test_conf_file_fixture]), ConfigDictModule()])
        conf = injector.get(dict)

        with open(test_conf_file_fixture, 'rt') as fd:
            origin_conf = json.load(fd)

        assert origin_conf == conf

    def test_config_injection_default(self, test_conf_file_fixture: str):
        # caplog.set_level(level=logging.DEBUG)

        dummy_conf = {"default_key": "default_value"}
        dummy_conf_fd, dummy_conf_file = tempfile.mkstemp()
        with open(dummy_conf_fd, 'wt') as fs:
            fs.write(json.dumps(dummy_conf))

        with mockito.when(ConfigLoader).get_default_conf_file().thenReturn(ic(dummy_conf_file)):
            injector = Injector([config_bind_factory(), ConfigDictModule()])
            conf = injector.get(dict)
            assert dummy_conf == conf

            mockito.verify(ConfigLoader, times=1).get_default_conf_file()

    def test_config_injection_with_multi_conf_files(self, test_conf_file_fixture: str):
        dummy_conf = {"dummy_key": "dummy_value"}
        dummy_conf_fd, dummy_conf_file = tempfile.mkstemp()
        with open(dummy_conf_fd, 'wt') as fs:
            fs.write(json.dumps(dummy_conf))

        injector = Injector(
            modules=[config_bind_factory(conf_files=[test_conf_file_fixture, dummy_conf_file]),
                     ConfigDictModule()])
        conf = injector.get(dict)

        with open(test_conf_file_fixture, 'rt') as fs:
            test_conf = json.load(fs)

        test_conf.update(dummy_conf)
        assert conf == test_conf

    def test_config_injection_with_overwrite(self, test_conf_file_fixture: str):
        with open(test_conf_file_fixture, 'rt') as fs:
            test_conf = json.load(fs)

        overwrite_key = list(test_conf.keys())[0]
        overwrite_value = "overwritten by {0}_{1}".format(
            self.test_config_injection_with_overwrite.__name__, self.__hash__())
        dummy_conf_file, dummy_conf = self.prep_dummy_config_file(items={overwrite_key: overwrite_value})

        injector = Injector(
            modules=[config_bind_factory(conf_files=[test_conf_file_fixture, dummy_conf_file]),
                     ConfigDictModule()])
        conf = injector.get(dict)
        assert conf.pop(overwrite_key) == overwrite_value
        dummy_conf.pop(overwrite_key)
        test_conf.pop(overwrite_key)
        test_conf.update(dummy_conf)
        assert conf == test_conf

    def test_config_injection_with_phantom_conf_file(self, caplog):
        caplog.set_level(level=logging.DEBUG)

        phantom_conf_file = tempfile.mktemp()
        injector = Injector(modules=[config_bind_factory(conf_files=[phantom_conf_file]), ConfigDictModule()])
        with pytest.raises(FileNotFoundError) as excpt_info:
            conf = injector.get(dict)

        assert excpt_info.value.args[0] == 2
        assert excpt_info.value.args[1] == 'No such file or directory'
        assert str(excpt_info.value).find(phantom_conf_file)

    def test_config_injector_factory(self, test_conf_file_fixture: str):
        injector = config_injector_factory(conf_files=[test_conf_file_fixture])
        conf = injector.get(dict)

        with open(test_conf_file_fixture, 'rt') as fd:
            origin_conf = json.load(fd)

        assert origin_conf == conf

    def test_config_injector_factory_default(self, test_conf_file_fixture: str):
        dummy_conf_file, dummy_conf = self.prep_dummy_config_file()

        with mockito.when(ConfigLoader).get_default_conf_file().thenReturn(ic(dummy_conf_file)):
            injector = config_injector_factory()
            conf = injector.get(dict)
            assert dummy_conf == conf

            mockito.verify(ConfigLoader, times=1).get_default_conf_file()

    def test_config_injector_factory_with_overwrite(self, test_conf_file_fixture: str):
        with open(test_conf_file_fixture, 'rt') as fs:
            test_conf = json.load(fs)

        overwrite_key = list(test_conf.keys())[0]
        overwrite_value = "overwritten by {0}_{1}".format(
            self.test_config_injection_with_overwrite.__name__, self.__hash__())
        dummy_conf_file, dummy_conf = self.prep_dummy_config_file(items={overwrite_key: overwrite_value})

        injector = config_injector_factory(conf_files=[test_conf_file_fixture, dummy_conf_file])
        conf = injector.get(dict)
        assert conf.pop(overwrite_key) == overwrite_value
        dummy_conf.pop(overwrite_key)
        test_conf.pop(overwrite_key)
        test_conf.update(dummy_conf)
        assert conf == test_conf
