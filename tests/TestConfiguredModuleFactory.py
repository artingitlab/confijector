#!/usr/bin/env python3
# -*- coding: utf-8 -*-
from typing import Dict, Type

import pytest
from injector import Injector, Module, provider

class ConfiguredModuleFactory:
    def __init__(self, clazz: type, config: Dict):
        self.clazz = type
        self.config = config

    def gen_module(self) -> Type:
        class ConfiguredModule(Module):
            def __init__(self, clazz: type, config: Dict):
                self.clazz = clazz
                self.config = config

            # This didn't work; return type needs to be explicitly specified
            @provider
            def provide_configured(self):
                return self.clazz(**self.config)

        return ConfiguredModule


sample_config = {
    "foo": {
        "bar": "baz"
    },
    "hoo": {
        "bee": "boo"
    }
}


class Foo:
    def __init__(self, bar: str):
        self.bar = bar


@pytest.mark.skip(reason="ConfiguredModuleFactory class design is not functional.")
def test_ConfiguredModuleFactory():
    foo_module = ConfiguredModuleFactory(clazz=Foo, config=sample_config["foo"]).gen_module()
    injector = Injector([foo_module])
    foo = injector.get(Foo)
    assert foo.bar == sample_config["foo"]["bar"]
