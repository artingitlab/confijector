#!/usr/bin/env python3
# -*- coding: utf-8 -*-


from injector import inject, provider, singleton
import logging
from logear import AutoLogException
import operator
from typing import cast, Dict, Iterable, List, Optional, Type, Union

from confijector.ConfigLoader import ConfigDictModule


def shall_wrap(clz: Type, callee_name: str) -> bool:
    if callee_name.startswith('_'):
        return False
    if callee_name in ["get_1st_item_value", "update_1st_item_value"]:
        return False
    return True


@AutoLogException.adapt_autolog_exception(shall_wrap=shall_wrap)
class ConfigParser:
    """
    A utility class for parsing and manipulating configurations stored in dictionary format.

    This class offers functionalities to access, search, update, and manage nested configurations effectively.
    It supports operations like fetching the first occurrence of a value by key, updating values, and retrieving
    all values matching a specified key across nested structures. Additional utility methods provide capabilities
    for flattening nested dictionaries and accessing values through a sequence of keys.
    """

    @inject
    def __init__(self, config: dict):
        """
        Initializes a ConfigParser instance with a configuration dictionary.

        The configuration dictionary can include nested dictionaries, allowing for structured configuration management.

        Parameters:
        - config (dict): The configuration dictionary to be managed by this instance.

        Example:
        ```
        config = {'database': {'host': 'localhost', 'port': 3306}}
        config_parser = ConfigParser(config=config)
        ```
        """

        self._config = config

    @property
    def config(self) -> dict:
        """
        A property function that returns the config dictionary.
        """
        return self._config

    @staticmethod
    def get_1st_item_value(dict_obj: dict, key: str):
        """
        Searches for the first occurrence of a given key in a nested dictionary and returns its value.

        This method recursively searches through all levels of nested dictionaries. If the key is found, the search
        stops, and the associated value is returned. If the key does not exist, None is returned.

        Parameters:
        - dict_obj (dict): The nested dictionary to search.
        - key (str): The key to search for.

        Returns:
        - The value associated with the first occurrence of the key, or None if the key is not found.

        Example:
        ```
        config = {'server': {'host': 'localhost', 'port': 8080}}
        value = ConfigParser.get_1st_item_value(config, 'port')
        # value would be 8080
        ```
        """

        if key in dict_obj:
            return dict_obj[key]
        for k, v in dict_obj.items():
            if isinstance(v, dict):
                found = ConfigParser.get_1st_item_value(dict_obj=v, key=key)
                if found is not None:
                    return found
        return None

    def get_1st_config_value(self, key: Union[str, int, float]):
        """
        Retrieves the value associated with the first occurrence of the specified key within the config dictionary.

        This method extends `get_1st_item_value` by applying it directly to the instance's config dictionary, allowing
        for an easy way to retrieve values without directly manipulating the dictionary.

        Parameters:
        - key (Union[str, int, float]): The key to search for in the config dictionary.

        Returns:
        - The value associated with the first occurrence of the key, or None if the key is not found.

        Example:
        ```
        config_parser = ConfigParser({'settings': {'resolution': '1080p'}})
        value = config_parser.get_1st_config_value('resolution')
        # value would be '1080p'
        ```
        """
        return ConfigParser.get_1st_item_value(dict_obj=self._config, key=key)

    # TODO: implement method to override a node (sub-dictionary) found first by key
    @staticmethod
    def update_1st_item_value(dict_obj: dict, key: str, value: Union[str, int, float, bool, Iterable]) -> bool:
        """
        Updates the first found occurrence of the specified key in a nested dictionary with the provided value.

        This method searches the nested dictionary for the key and updates the first occurrence's value. If the key
        is updated successfully, the method returns True. If the key is not found, the method returns False, indicating
        that no update was performed.

        Parameters:
        - dict_obj (dict): The nested dictionary to update.
        - key (str): The key to search for and update.
        - value (Union[str, int, float, bool, Iterable]): The new value to set for the found key.

        Returns:
        - bool: True if the key was found and updated; False otherwise.

        Example:
        ```
        config = {'server': {'host': 'localhost', 'port': 8080}}
        updated = ConfigParser.update_1st_item_value(config, 'port', 9090)
        # updated would be True
        ```
        """

        if key in dict_obj:
            dict_obj[key] = value
            return True
        for k, v in dict_obj.items():
            if isinstance(v, dict):
                updated = ConfigParser.update_1st_item_value(dict_obj=v, key=key, value=value)
                if updated:
                    return True
        return False

    def update_1st_config_value(self,
                                key: Union[str, int, float], value: Union[str, int, float, bool, Iterable]) -> bool:
        """
        Updates the value associated with the first occurrence of the specified key within the instance's configuration.

        This method is useful for modifying configurations dynamically, especially in nested dictionaries where direct
        access might be cumbersome. It returns a boolean indicating whether the update was successful.

        Parameters:
        - key (Union[str, int, float]): The key whose value should be updated.
        - value (Union[str, int, float, bool, Iterable]): The new value to assign to the key.

        Returns:
        - bool: True if the key was found and successfully updated; False otherwise.

        Example:
        ```
        config_parser = ConfigParser({'server': {'port': 8080}})
        success = config_parser.update_1st_config_value('port', 9090)
        # success would be True
        ```
        """
        return ConfigParser.update_1st_item_value(dict_obj=self._config, key=key, value=value)

    @staticmethod
    def get_items_values(dict_obj: dict, key: str) -> Dict:
        """
        Recursively searches for and retrieves values associated with a specified key in a nested dictionary.

        This method traverses a nested dictionary (`dict_obj`) to find all occurrences of a specified key (`key`).
        If the key is found, the method records the value associated with this key along with the hierarchical path
        leading to the key as the path's representation (keys joined by commas). This allows for identification
        of values by their unique paths in the nested structure. The search is exhaustive and includes all levels
        of nesting within the input dictionary.

        Parameters:
        - dict_obj (dict): The dictionary to search through. It can be a nested dictionary containing other dictionaries.
        - key (str): The key to search for in the dictionary. The search is case-sensitive.

        Returns:
        - Dict: A dictionary where each key is a comma-separated string representing the unique path to the found key
          within the original dictionary, and each value is the value associated with the found key. If the key is not
          found at any level, an empty dictionary is returned.

        Examples:
        ```
        input_dict = {
            "level1": {
                "level2": {
                    "target": "value1"
                },
                "target": "value2"
            },
            "target": "value3"
        }
        result = ClassName.get_items_values(input_dict, "target")
        # Result would be:
        # {
        #   "level1,level2,target": "value1",
        #   "level1,target": "value2",
        #   "target": "value3"
        # }
        ```

        Note:
        - This method is static, meaning it should be called on the class rather than an instance of the class.
        - The order of keys in paths and the returned dictionary does not follow a specific order and is dependent
          on the order of keys as iterated in the dictionary.
        """

        key_sequence = []
        found = {}

        def _do_get_sub_nodes(dict_obj: dict, key: str):
            for k, v in dict_obj.items():
                if key == k:
                    key_sequence.append(k)
                    found.update({",".join(key_sequence): v})
                    key_sequence.pop()
                    continue
                if isinstance(v, dict):
                    key_sequence.append(k)
                    _do_get_sub_nodes(dict_obj=v, key=key)
                    key_sequence.pop()

        _do_get_sub_nodes(dict_obj=dict_obj, key=key)
        return found

    def get_configs_values(self, key: str) -> Dict:
        """
        Retrieves all values associated with the specified key within the instance's config dictionary.

        This method leverages `get_items_values` to search the entire configuration for the given key, returning
        a dictionary of all matched values and their corresponding paths.

        Parameters:
        - key (str): The key to search for in the configuration.

        Returns:
        - Dict: A dictionary with keys representing the paths to the found values, and values being the associated
          values found at those paths.

        Example:
        ```
        config_parser = ConfigParser({'user': {'name': 'John', 'age': 30}})
        values = config_parser.get_configs_values('name')
        # values might be {'user,name': 'John'}
        ```
        """

        return ConfigParser.get_items_values(dict_obj=self._config, key=key)

    @staticmethod
    def flatten_dict(dict_obj: dict, sep: str = ',') -> Dict:
        """
        A function to recursively flatten a dictionary object with nested keys into a single-level dictionary.

        Parameters:
            dict_obj (dict): The dictionary object to be flattened.
            sep (str): The separator to use between nested keys. Default is ','.

        Returns:
            Dict: A flattened dictionary object with single-level keys.
        """

        flattened = {}

        def do_flatten_dict(dict_obj: dict, sep: str = ',', parent_key: str = ''):
            for k, v in dict_obj.items():
                new_key = parent_key + sep + k if parent_key else k
                if isinstance(v, dict):
                    do_flatten_dict(dict_obj=v, sep=sep, parent_key=new_key)
                else:
                    flattened.update({new_key: v})

        do_flatten_dict(dict_obj=dict_obj, sep=sep)
        return flattened

    @staticmethod
    def get_value_by_keys(dict_obj: dict, key_sequence: List):
        """
        Retrieves a value from a nested dictionary using a sequence of keys.

        This method allows for deep retrieval of a value using a list of keys which represent the path through the
        nested dictionary. If any key in the sequence is not found, a KeyError is raised.

        Parameters:
        - dict_obj (dict): The nested dictionary to search through.
        - key_sequence (List): A list of keys representing the path to the desired value.

        Returns:
        - The value found at the specified path.

        Example:
        ```
        config = {'server': {'database': {'host': 'localhost'}}}
        value = ConfigParser.get_value_by_keys(config, ['server', 'database', 'host'])
        # value would be 'localhost'
        ```

        Raises:
        - KeyError: If any key in the sequence is not found in the dictionary path.
        """

        _keys = key_sequence.copy()
        value = dict_obj
        while len(_keys) > 0:
            value = operator.itemgetter(_keys.pop(0))(value)
        return value

    def get_config_value_by_keys(self,
                                 key_sequence: List,
                                 default: Optional[Union[int, float, str, Iterable]]=None):
        """
        Retrieves a value from the instance's config dictionary based on a sequence of keys.

        This method provides a way to deeply navigate through nested configurations using a list of keys, returning
        the value at the nested location specified by the sequence of keys.

        Parameters:
        - key_sequence (List): A list of keys representing the path through the nested dictionary to the desired value.
        - default (int, float, str, or Iterable): An optional default value to return if the key sequence is not found.

        Returns:
        - The value found at the specified path or value given by default parameter if the complete path is not found.

        Example:
        ```
        config_parser = ConfigParser({'server': {'database': {'host': 'localhost'}}})
        host = config_parser.get_config_value_by_keys(['server', 'database', 'host'])
        # host would be 'localhost'
        """

        try:
            return ConfigParser.get_value_by_keys(dict_obj=self._config, key_sequence=key_sequence)
        except Exception as e:
            cast(logging.Logger, self.grab_logger()).info(
                "Returning {0} since locating key sequence {1} failed: {2}".format(default, key_sequence, e))
            return default

    # TODO: implement method to override a node (sub-dictionary) found recursively by key


class ConfigParserModule(ConfigDictModule):
    """
    A module class extending ConfigDictModule to provide a singleton ConfigParser instance.

    This class is designed to integrate with dependency injection frameworks, such as the `injector` package, to
    supply a shared instance of ConfigParser across an application. The ConfigParser instance is initialized with
    a configuration dictionary, facilitating centralized configuration management.

    Methods:
        provide_configparser(self, config: dict) -> ConfigParser: A provider method annotated with @singleton
            to ensure a single instance of ConfigParser is created and injected wherever needed. The method
            initializes ConfigParser with the provided configuration dictionary.
    """

    @singleton
    @provider
    def provide_configparser(self, config: dict) -> ConfigParser:
        return ConfigParser(config=config)
