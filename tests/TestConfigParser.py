#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from injector import Injector
import json
import pytest
import tempfile

from confijector.ConfigLoader import ConfigDictModule, config_bind_factory
from confijector.ConfigParser import ConfigParser, ConfigParserModule


@pytest.fixture(scope='class')
def conf_file_for_test(request):
    """
    Create a temporary .config.json file for testing.
    :param request:
    :return:
    """
    data1 = {
        'a': 1,
        'b': {
            'c': 2,
            'd': {
                'e': 5
            }
        },
        'c': 3,
        'd': 4,
    }

    with tempfile.NamedTemporaryFile(mode='w', delete=False) as fd:
        json.dump(data1, fd)

    request.cls.test_config_file = fd.name
    request.cls.test_data1 = data1
    # yield
    # os.remove(fd.name)


@pytest.fixture()
def conf_data_for_test(request):
    with open(request.cls.test_config_file, 'rt') as fd:
        request.cls.test_data1 = json.load(fd)


@pytest.fixture()
def conf_injector_for_test(request):
    injector = Injector(modules=[
        config_bind_factory(conf_files=[request.cls.test_config_file]),
        ConfigDictModule(),
        ConfigParserModule()])
    request.cls.injector = injector


@pytest.mark.usefixtures('conf_file_for_test', 'conf_data_for_test', 'conf_injector_for_test')
class TestConfigParser:
    def test_get_1st_item_value(self):
        assert ConfigParser.get_1st_item_value(dict_obj=self.test_data1, key='a') == self.test_data1['a']
        assert ConfigParser.get_1st_item_value(dict_obj=self.test_data1, key='b') == self.test_data1['b']
        assert ConfigParser.get_1st_item_value(dict_obj=self.test_data1, key='d') == self.test_data1['d']
        assert ConfigParser.get_1st_item_value(dict_obj=self.test_data1, key='e') == self.test_data1['b']['d']['e']
        assert ConfigParser.get_1st_item_value(dict_obj=self.test_data1, key='f') is None

    def test_get_1st_config_value(self):
        cnfparser = self.injector.get(ConfigParser)

        assert cnfparser.get_1st_config_value(key='a') == cnfparser.config['a']
        assert cnfparser.get_1st_config_value(key='b') == cnfparser.config['b']
        assert cnfparser.get_1st_config_value(key='d') == cnfparser.config['d']
        assert cnfparser.get_1st_config_value(key='e') == cnfparser.config['b']['d']['e']
        assert cnfparser.get_1st_config_value(key='f') is None

    def test_update_1st_item_value(self):
        assert not ConfigParser.update_1st_item_value(dict_obj=self.test_data1, key='f', value=50)
        assert ConfigParser.get_1st_item_value(dict_obj=self.test_data1, key='f') is None
        assert ConfigParser.update_1st_item_value(dict_obj=self.test_data1, key='e', value=40)
        assert self.test_data1['b']['d']['e'] == 40
        assert ConfigParser.update_1st_item_value(dict_obj=self.test_data1, key='d', value=30)
        assert self.test_data1['d'] == 30
        assert ConfigParser.update_1st_item_value(dict_obj=self.test_data1, key='b', value=20)
        assert self.test_data1['b'] == 20
        assert ConfigParser.update_1st_item_value(dict_obj=self.test_data1,
                                                  key='a',
                                                  value={
                                                      'b': {
                                                          'c': 30
                                                      }})
        assert self.test_data1['a'] == \
               {
                   'b': {
                       'c': 30
                   }
               }

    def test_update_1st_config_value(self):
        cnfparser = self.injector.get(ConfigParser)

        assert not cnfparser.update_1st_config_value(key='f', value=500)
        assert cnfparser.update_1st_config_value(key='e', value=400)
        assert cnfparser.config['b']['d']['e'] == 400
        assert cnfparser.update_1st_config_value(key='d', value=300)
        assert cnfparser.config['d'] == 300
        assert cnfparser.update_1st_config_value(key='b', value=200)
        assert cnfparser.config['b'] == 200
        assert cnfparser.update_1st_config_value(key='a',
                                                 value={
                                                     'b': {
                                                         'c': 300
                                                     }})
        assert cnfparser.config['a'] == \
               {
                   'b': {
                       'c': 300
                   }
               }

    def test_get_items_values(self):
        found = ConfigParser.get_items_values(dict_obj=self.test_data1, key='d')
        assert len(found) == 2
        assert list(found.keys()) == ['b,d', 'd']
        assert list(found.values()) == [{'e': 5}, 4]

    def test_get_configs_values(self):
        cnfparser = self.injector.get(ConfigParser)

        found = cnfparser.get_configs_values(key='d')
        assert len(found) == 2
        assert list(found.keys()) == ['b,d', 'd']
        assert list(found.values()) == [{'e': 5}, 4]

    def test_flatten_dict(self):
        flattened = ConfigParser.flatten_dict(dict_obj=self.test_data1)
        assert len(flattened) == 5
        assert flattened['a'] == 1
        assert flattened['b,c'] == 2
        assert flattened['b,d,e'] == 5
        assert flattened['c'] == 3
        assert flattened['d'] == 4

    def test_get_value_by_keys(self):
        assert ConfigParser.get_value_by_keys(dict_obj=self.test_data1, key_sequence=['b', 'd', 'e']) \
               == self.test_data1['b']['d']['e']

    def test_get_config_value_by_keys(self):
        cnfparser = self.injector.get(ConfigParser)

        found = cnfparser.get_config_value_by_keys(key_sequence=['b', 'd', 'e'])
        assert found == self.test_data1['b']['d']['e']

        found = cnfparser.get_config_value_by_keys(key_sequence=['b', 'd', 'f'])
        assert found is None

        default_value = 'default value'
        found = cnfparser.get_config_value_by_keys(key_sequence=['b', 'd', 'f'],
                                                   default=default_value)
        assert found == default_value
